solution "Pinyin"
	location ( "build" )
	configurations { "Debug", "Release" }
	platforms { "Win32", "Win64", "Linux" }
	
filter { "platforms:Win32" }
    system "windows"
    architecture "x32"
	defines { "_WIN32","WIN32","NOMINMAX"," _CRT_SECURE_NO_WARNINGS"}
	
filter { "platforms:Win64" }
    system "windows"
    architecture "x64"
	
	defines { "_WIN32","WIN32","NOMINMAX"," _CRT_SECURE_NO_WARNINGS"}
	
filter { "platforms:Linux" }
    system "linux"
    architecture "x64"	
	defines { "LINUX", "linux" ,"POSIX"}
	

filter "configurations:Debug"
	defines { "DEBUG" , "_DEBUG"}
	optimize "Debug"
	
filter "configurations:Release"
	defines { "NDEBUG" }
	optimize "Speed"
	vectorextensions "SSE2"	


project "test"
	language "C++"
	kind "ConsoleApp"
	includedirs{
		"include"
	}
	files{
		"*.h",
		"*.c",
	}

