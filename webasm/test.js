var Module = require('./pyconvert.js');

var py_convert = Module.cwrap("py_convert","string","number");

function to_pinyin(str){
	let list = [];
	for(let index =0;index<str.length;++index){
		let code = str.charCodeAt(index);
		if(code > 255)
			list.push(py_convert(code));
		else
			list.push(String.fromCharCode(code));
	}
	return list;
}


Module.onRuntimeInitialized = () => {
	let list = to_pinyin("汉语拼音");
	console.info(list);
}


